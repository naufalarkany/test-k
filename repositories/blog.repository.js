const Model = require("../db/mongo/Blog");
exports.create = async (newBlog) =>
  new Promise((resolve, reject) => {
    const blog = new Model(newBlog);
    blog
      .save()
      .then((data) => {
        resolve(data);
      })
      .catch(reject);
  });

exports.findAll = async () =>
  new Promise((resolve, reject) => {
    Model.find()
      .then((data) => {
        resolve(data);
      })
      .catch(reject);
  });

exports.findById = async (id) =>
  new Promise((resolve, reject) => {
    Model.findById(id)
      .then((data) => {
        if (!data) {
          reject({ message: "Blog not found" });
        }
        resolve(data);
      })
      .catch(reject);
  });

exports.update = async (id, updatedData) =>
  new Promise((resolve, reject) => {
    Model.findByIdAndUpdate(id, updatedData, { useFindAndModify: false })
      .then((data) => {
        if (!data) {
          reject({ message: "Blog not found" });
        }
        resolve(data);
      })
      .catch(reject);
  });

exports.destroy = async (id) =>
  new Promise((resolve, reject) => {
    Model.findByIdAndDelete(id)
      .then((data) => {
        if (!data) {
          reject({ message: "Blog not found" });
        }
        resolve(data);
      })
      .catch(reject);
  });
