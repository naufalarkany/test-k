const auth = require("./authController");
const blog = require("./blogController");

module.exports = {
  auth,
  blog,
};
