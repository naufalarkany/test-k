const { Router } = require("express");
const { blog } = require("../controller");
const authenticationMiddleware = require("../middleware/auth");
const router = Router();

router.post("/", authenticationMiddleware, blog.create);
router.get("/", authenticationMiddleware, blog.findAll);
router.get("/:id", authenticationMiddleware, blog.findById);
router.patch("/:id", authenticationMiddleware, blog.update);
router.delete("/:id", authenticationMiddleware, blog.destroy);

module.exports = router;
