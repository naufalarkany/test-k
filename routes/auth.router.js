const { Router } = require("express");
const { auth } = require("../controller");
const authenticationMiddleware = require("../middleware/auth");
const isAdminMiddleware = require("../middleware/isAdmin");
const router = Router();

router.post("/register", authenticationMiddleware, isAdminMiddleware, auth.create);
router.post("/login", auth.login);

module.exports = router;
