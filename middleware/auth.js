const jwt = require("jsonwebtoken");
const repository = require("../repositories/user.repository");

const authenticationMiddleware = async (req, res, next) => {
  const authHeader = req.headers.authorization;
  if (!authHeader || !authHeader.startsWith("Bearer ")) {
    return res.status(401).send({
      status: "failed",
      msg: "Not Authenticated",
    });
  }

  const token = authHeader.split(" ")[1];

  try {
    const decoded = jwt.verify(token, process.env.ACCESS_TOKEN_SECRET);
    const { id } = decoded;
    const user = await repository.findById(id);
    if (!user) {
      return res.status(401).send({
        status: "failed",
        msg: "Not Authenticated",
      });
    }
    req.user = user.toJSON();
    return next();
  } catch (error) {
    return res.status(401).send({
      status: "failed",
      msg: "Not Authenticated",
    });
  }
};

module.exports = authenticationMiddleware;
