const { Op } = require("sequelize");
const { User: Model } = require("../db/models");

exports.create = async (newUser) =>
  new Promise((resolve, reject) => {
    Model.create(newUser)
      .then((data) => {
        resolve(data);
      })
      .catch(reject);
  });

exports.findByUsername = async (username) =>
  new Promise((resolve, reject) => {
    Model.findOne({
      where: {
        username,
      },
    })
      .then((data) => {
        resolve(data);
      })
      .catch(reject);
  });

exports.findById = async (id) =>
  new Promise((resolve, reject) => {
    Model.findByPk(id)
      .then((data) => {
        resolve(data);
      })
      .catch(reject);
  });
