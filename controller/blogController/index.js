const repository = require("../../repositories/blog.repository");
const mongoose = require("mongoose");
const isValidId = (id, res) => {
  if (!id) {
    return res.status(400).send({
      status: "failed",
      msg: "Params Id is required",
    });
  }
  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(400).send({
      status: "failed",
      msg: "Id is not valid",
    });
  }
};
exports.findAll = async (req, res) => {
  try {
    const blogs = await repository.findAll();
    return res.status(200).send({
      status: "success",
      msg: "Successfully get Blogs",
      data: blogs,
    });
  } catch (error) {
    return res.status(500).send({
      status: "failed",
      msg: error.message,
    });
  }
};

exports.create = async (req, res) => {
  try {
    const { body } = req;
    const newBlog = {
      title: body.title,
      content: body.content,
      author: body.author,
      user_id: body.user_id,
    };
    const blogs = await repository.create(newBlog);
    return res.status(200).send({
      status: "success",
      msg: "Successfully get Blogs",
      data: blogs,
    });
  } catch (error) {
    return res.status(500).send({
      status: "failed",
      msg: error.message,
    });
  }
};

exports.findById = async (req, res) => {
  try {
    const { id } = req.params;
    isValidId(id, res);
    const blog = await repository.findById(req.params.id);
    return res.status(200).send({
      status: "success",
      msg: "Successfully get Blog",
      data: blog,
    });
  } catch (error) {
    return res.status(500).send({
      status: "failed",
      msg: error.message,
    });
  }
};

exports.update = async (req, res) => {
  try {
    const { id } = req.params;
    isValidId(id, res);
    const { body } = req;
    const blog = await repository.update(req.params.id, body);
    return res.status(200).send({
      status: "success",
      msg: "Successfully update Blog",
      data: blog,
    });
  } catch (error) {
    return res.status(500).send({
      status: "failed",
      msg: error.message,
    });
  }
};

exports.destroy = async (req, res) => {
  try {
    const { id } = req.params;
    isValidId(id, res);
    await repository.destroy(req.params.id);
    return res.status(200).send({
      status: "success",
      msg: "Successfully deleted Blog",
    });
  } catch (error) {
    return res.status(500).send({
      status: "failed",
      msg: error.message,
    });
  }
};
