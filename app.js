const express = require("express");
const mongoose = require("mongoose");
const router = require("./routes");
const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

mongoose
  .connect(process.env.MONGO_URI)
  .then(() => console.log("mongoDB Connected"))
  .catch((err) => console.log(err));

require("dotenv").config();
require("./utils/runLogger")(app);

//use for main router
app.use("/api", router);

//just for test API
app.get("/health-check", (req, res) => {
  try {
    return res.status(200).send({
      status: "success",
      msg: "API running well",
    });
  } catch (error) {
    return res.status(500).send({
      status: "failed",
      msg: error.message,
    });
  }
});

// App starting here
const port = process.env.PORT;
const start = async () => {
  try {
    app.listen(port, () => console.log(`Server is listening on port ${port}...`));
  } catch (error) {
    console.log(error);
  }
};

start();
