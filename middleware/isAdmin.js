const isAdminValidation = async (req, res, next) => {
  try {
    if (!req.user.isAdmin) {
      return res.status(403).send({
        status: "failed",
        msg: "Not authorized to access this route",
      });
    }
    return next();
  } catch (error) {
    return res.status(403).send({
      status: "failed",
      msg: "Not authorized to access this route",
    });
  }
};

module.exports = isAdminValidation;
